const express = require('express');

const app = express();

const port = 3000;

// Allows your app to read json data
//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//GET route
app.get('/home',(req,res)=>{
	let message;
	message = `This is home`;
	res.send(message)
});

//port
app.listen(port, ()=> console.log(`Server running at ${port}`));

//mockdatabase
let users = [];


//Another get
app.get('/users',(req,res)=>{
	res.send(users)
});

//post
app.post('/addUsers',(req,res)=>{
	users.push(req.body);
	res.send(`User ${req.body.lastName} successfully registered`);
	console.log(users);
});